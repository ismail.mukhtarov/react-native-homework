import React, {useState} from 'react';
import {Provider} from 'react-redux';
import {AppLoading} from "expo";

import {RootDrawer} from "./navigation/RootDrawer";
import {loadFonts} from "./styles/fonts";
import store from './store';


export default function App() {
    const [loaded, setLoaded] = useState(false);
    if (!loaded) {
        return (
            <AppLoading
                startAsync={loadFonts}
                onFinish={() => setLoaded(true)}
                onError={() => console.log("Loading Rejected")}
            />
        );

    }
    return (
        <Provider store={store}>
            <RootDrawer/>
        </Provider>
    );
}

