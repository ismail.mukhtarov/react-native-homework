import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import {createDrawerNavigator} from "@react-navigation/drawer";

import {AuthStack} from "./AuthStack";
import {LeftDrawer} from "../components";
import {StatusBar} from "react-native";

const {Navigator, Screen} = createDrawerNavigator();

export const RootDrawer = () => {
    return (
        <NavigationContainer>
            <StatusBar barStyle="light-content"/>
            <Navigator drawerContent={(props) => <LeftDrawer {...props}/>}>
                <Screen name='Auth' component={AuthStack}/>
            </Navigator>
        </NavigationContainer>
    )
};