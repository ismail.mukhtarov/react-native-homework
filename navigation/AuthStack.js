import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";

import {OneTime, SingleList, SingleListEdit, UserSettings, NewList, Regular} from "../screens";

const {Navigator, Screen} = createStackNavigator();

export const AuthStack = () => {
    return (
        <Navigator headerMode='none'>
            <Screen name='OneTime' component={OneTime}/>
            <Screen name='Regular' component={Regular}/>
            <Screen name='SingleList' component={SingleList}/>
            <Screen name='SingleListEdit' component={SingleListEdit}/>
            <Screen name='UserSettings' component={UserSettings}/>
            <Screen name='NewList' component={NewList}/>
        </Navigator>
    )
};