import React from 'react';
import {StyleSheet, View, TouchableOpacity, Image} from 'react-native';

import {AntDesign, MaterialIcons} from '@expo/vector-icons';
import * as constant from "../styles/constants";
import {CustomText} from "../components";
import {SingleListItem} from "../components/SingleListItem";
import {changeItemBought} from "../store/project";
import {connect} from "react-redux";


export const SingleList = connect(null, {changeItemBought})((props) => {
    const shopListSection = props.route.params.shopListSection,
        shopList = props.route.params.shopList;

    const isBoughtChangeHandler = (itemId) => {
        props.changeItemBought({
            sectionName: shopListSection.name,
            listId: shopList.id,
            listItemId: itemId,
        })
    };


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.backIcon} onPress={() => props.navigation.goBack()}>
                    <AntDesign name="arrowleft" size={24} color="#FFFFFF"/>
                </TouchableOpacity>

                <CustomText weight='semi' style={styles.headerText}>{shopList.name}</CustomText>

                <TouchableOpacity style={styles.editIcon}
                                  onPress={() => props.navigation.navigate('SingleListEdit', {
                                      shopListSection,
                                      shopList: shopList,
                                  })}>
                    <MaterialIcons name="edit" size={24} color="#FFFFFF"/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <View style={styles.contentView}>
                    <TouchableOpacity style={styles.contentBtn}>
                        <CustomText weight='bold' style={{fontSize: 10, color: constant.clrWhite}}>RESET</CustomText>
                    </TouchableOpacity>
                    <CustomText style={{
                        fontSize: 14,
                        color: constant.clrBlack
                    }}>{props.route.params.boughtNumber} / {props.route.params.totalNumber}</CustomText>
                </View>
                {
                    shopList.shopListItems.map((listItem) => (
                        <SingleListItem onLongPress={() => isBoughtChangeHandler(listItem.id)}
                                        key={listItem.id}
                                        id={listItem.id}
                                        name={listItem.name}
                                        amount={listItem.amount}
                                        unit={listItem.unit}
                                        isBought={listItem.isBought}/>
                    ))
                }
            </View>

        </View>
    )
});

const styles = StyleSheet.create({
        header: {
            backgroundColor: constant.clrOrange,
            height: constant.windowHeight / 2,
            width: constant.windowWidth,
        },
        content: {
            position: 'absolute',
            flex: 1,
            alignItems: 'center',
            backgroundColor: constant.clrWhite,
            height: constant.windowHeight,
            width: constant.windowWidth,
            borderTopEndRadius: 20,
            borderTopStartRadius: 20,
            top: 70,
            paddingTop: 10,
        },

        backIcon: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            width: 100,
            height: 80,
            top: -3,
            left: -22,
        },

        editIcon: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            color: 'black',
            width: 100,
            height: 80,
            top: -3,
            right: -22,
        },

        headerText: {
            fontSize: 18,
            textAlign: "center",
            color: constant.clrWhite,
            paddingTop: 25,
            lineHeight: 22,
        },

        contentView: {
            width: constant.windowWidth - 30,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
        },

        contentBtn: {
            backgroundColor: constant.clrOrange,
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
            width: 72,
            height: 20,
            borderRadius: 50,
        }
    })
;