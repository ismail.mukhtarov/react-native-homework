import React from 'react';
import {Alert, StyleSheet, View} from 'react-native';
import {connect} from "react-redux";

import {CardsList, LeftDrawerNavigateBtn, CustomText} from "../components";
import * as constant from "../styles/constants";
import {deleteList, getShopLists} from "../store/project";


const mapStateToProps = (state) => ({
    shopListSections: getShopLists(state),
});


export const OneTime = connect(mapStateToProps, {deleteList})((props) => {
    const {shopListSections} = props;

    const deleteSectionList = (listName, listId) => {
        Alert.alert(
            "Delete list?",
            `Are you sure what you want delete "${listName}"?`, [
                {
                    text: 'Cancel',
                    style: 'cancel'
                },
                {
                    text: 'Yes',
                    onPress: () => props.deleteList({sectionName: "ONE_TIME", listId})
                }
            ])
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <CustomText weight='medium' style={styles.headerText}>One Time Lists</CustomText>
                <LeftDrawerNavigateBtn onPress={() => props.navigation.openDrawer()}/>
            </View>
            <View style={styles.content}>

                <CardsList allBoughtOpacity={true}
                           deleteSectionList={deleteSectionList}
                           section={shopListSections.find(section => section.name === 'ONE_TIME')}
                           navigation={props.navigation}
                />
            </View>

        </View>
    )
});

const styles = StyleSheet.create({
    header: {
        backgroundColor: constant.clrOrange,
        height: constant.windowHeight / 2,
        width: constant.windowWidth,
    },
    content: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        backgroundColor: constant.clrWhite,
        height: constant.windowHeight,
        width: constant.windowWidth,
        borderTopEndRadius: 20,
        borderTopStartRadius: 20,
        top: 70,
    },
    headerText: {
        textAlign: "center",
        fontSize: 16,
        color: constant.clrWhite,
        paddingTop: 27,
        lineHeight: 20,
    }
});