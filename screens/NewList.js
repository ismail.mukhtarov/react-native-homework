import React, {useState} from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {connect} from 'react-redux'

import * as constant from '../styles/constants'
import {CustomBtn, CustomInput, CustomText} from '../components';
import {addList} from "../store/project";


export const NewList = connect(null, {addList})((props) => {
    const [newList, setNewList] = useState({
        listName: '',
        sectionName: '',
    });

    const fieldHandler = (valueName, value) => {
        setNewList((newList) => ({
            ...newList,
            [valueName]: value,
        }));
    };

    const creatNewList = () => {
        for (let key in newList) {
            if (newList[key].trim() === "") {
                return;
            }
        }
        props.addList(newList);
        props.navigation.navigate('OneTime');
    };

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <CustomText weight='semi' style={styles.headerText}>New List</CustomText>
            </View>
            <View style={styles.content}>
                <CustomText weight='semi' style={{fontSize: 12, opacity: 0.5,}}>list name</CustomText>
                <CustomInput action={fieldHandler} valueName='listName' btnWidth={constant.windowWidth - 30}
                             children={newList.listName}/>
                <View style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: constant.windowWidth - 30,
                }}>
                    <CustomBtn onPress={() => fieldHandler('sectionName', 'ONE_TIME')}
                               textClr={'black'}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               btnWidth={(constant.windowWidth / 2) - 30}
                               opacity={newList.sectionName === 'ONE_TIME' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>One Time</CustomText>
                    </CustomBtn>
                    <CustomBtn onPress={() => fieldHandler('sectionName', 'REGULAR')}
                               textClr={'black'}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               btnWidth={(constant.windowWidth / 2) - 30}
                               opacity={newList.sectionName === 'REGULAR' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>Regular</CustomText>
                    </CustomBtn>
                </View>
                <CustomBtn btnWidth={constant.windowWidth - 30}
                           onPress={() => creatNewList()}>
                    <CustomText weight='bold' style={{fontSize: 14,}}>CREATE LIST</CustomText>
                </CustomBtn>
            </View>
        </View>
    )
});

const styles = StyleSheet.create({
    header: {
        backgroundColor: constant.clrOrange,
        height: constant.windowHeight / 2,
        width: constant.windowWidth,
    },
    content: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        backgroundColor: constant.clrWhite,
        height: constant.windowHeight,
        width: constant.windowWidth,
        borderTopEndRadius: 20,
        borderTopStartRadius: 20,
        top: 70,
        paddingTop: 10,
    },
    headerText: {
        fontSize: 18,
        textAlign: "center",
        color: constant.clrWhite,
        paddingTop: 25,
        lineHeight: 22,
    }
});
