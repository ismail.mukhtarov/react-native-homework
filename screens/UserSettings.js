import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {connect} from "react-redux";


import * as constant from "../styles/constants";
import {CustomBtn, CustomInput, CustomText} from "../components";
import {updateUser} from "../store/settings";


export const UserSettings = connect(null, {updateUser})((props) => {
    const [userInfo, setUserInfo] = useState({
        name: '',
        url: '',
    });

    //updateUser function
    const updateUserInfo = (valueName, value) => {
        setUserInfo({
            ...userInfo,
            [valueName]: value,
        });
    };

    //submitForm function
    const submitForm = () => {
        // for (let key in userInfo) {
        //     if (userInfo[key].trim() === "") {
        //         return;
        //     }
        // }
        props.updateUser(userInfo);
        props.navigation.navigate('OneTime');
    };


    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <CustomText weight='semi' style={styles.headerText}>UserSettings</CustomText>
            </View>
            <View style={styles.content}>

                <CustomText weight='semi' style={styles.customText}>username</CustomText>
                <CustomInput action={updateUserInfo} valueName='name' btnWidth={constant.windowWidth - 30}
                             children={userInfo.name}/>

                <CustomText weight='semi' style={styles.customText}>avatar url</CustomText>
                <CustomInput action={updateUserInfo} valueName='url' btnWidth={constant.windowWidth - 30}
                             children={userInfo.url}/>

                <CustomBtn onPress={submitForm} bgColor={constant.clrOrange} textColor={constant.clrWhite}
                           btnWidth={constant.windowWidth - 30}
                           btnHeight={42}>
                    <CustomText weight='bold' style={{fontSize: 14,}}>SAVE CHANGES</CustomText>
                </CustomBtn>
            </View>
        </View>

    )
});

const styles = StyleSheet.create({
    header: {
        backgroundColor: constant.clrOrange,
        height: constant.windowHeight / 2,
        width: constant.windowWidth,
    },
    content: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        backgroundColor: constant.clrWhite,
        height: constant.windowHeight,
        width: constant.windowWidth,
        borderTopEndRadius: 20,
        borderTopStartRadius: 20,
        top: 70,
        paddingTop: 10,
    },

    customText: {
        marginTop: 9,
        fontSize: 12,
        opacity: 0.5,
    },

    headerText: {
        fontSize: 18,
        textAlign: "center",
        color: constant.clrWhite,
        paddingTop: 25,
        lineHeight: 22,
    }
});