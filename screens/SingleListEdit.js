import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity, ScrollView} from 'react-native';
import {AntDesign} from '@expo/vector-icons';
import {connect} from "react-redux";

import * as constant from "../styles/constants";
import {CustomBtn, CustomInput, CustomText, SingleListEditItem} from "../components";
import {addItemToList} from "../store/project";


export const SingleListEdit = connect(null, {addItemToList})((props) => {
    const shopList = props.route.params.shopList,
        shopListSection = props.route.params.shopListSection;


    const [fields, setFields] = useState({
        shopListSectionID: shopListSection.id,
        shopListID: shopList.id,
        itemName: '',
        itemCount: 0,
        itemUnit: 'pkg',
    });

    const fieldHandler = (valueName, value) => {
        setFields((fields) => ({
            ...fields,
            [valueName]: value,
        }));
    };

    const INCREASE_COUNT = () => {
        setFields({
            ...fields,
            itemCount: fields.itemCount + 1
        })
    };

    const DECREASE_COUNT = () => {
        setFields({
            ...fields,
            itemCount: fields.itemCount === 0 ? fields.itemCount : fields.itemCount - 1
        })
    };

    const addItem = () => {
        // for (let key in fields) {
        //     if (fields[key].trim() === "") {
        //         return;
        //     }
        // }
        if (fields.itemCount === 0) {
            return
        }
        props.addItemToList(fields);

        setFields({
            shopListSectionID: shopListSection.id,
            shopListID: shopList.id,
            itemName: '',
            itemCount: 0,
            itemUnit: 'pkg',
        })

    };
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.backIcon} onPress={() => props.navigation.navigate('OneTime')}>
                    <AntDesign name="arrowleft" size={24} color="#FFFFFF"/>
                </TouchableOpacity>

                <CustomText weight='semi' style={styles.headerText}>{shopList.name}</CustomText>

                <TouchableOpacity style={styles.editIcon} onPress={() => props.navigation.navigate('OneTime')}>
                    <AntDesign name="save" size={24} color={constant.clrWhite}/>
                </TouchableOpacity>
            </View>
            <View style={styles.content}>
                <View style={styles.contentView}>

                    {/*------------------itemName--------------*/}
                    <View style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                        <CustomText weight='semi'
                                    style={{
                                        fontSize: 12,
                                        opacity: 0.75,
                                        color: constant.clrBlack,
                                        marginTop: 9
                                    }}>
                            position name
                        </CustomText>
                        <CustomInput children={fields.itemName}
                                     action={fieldHandler}
                                     valueName='itemName'
                                     btnWidth={constant.windowWidth - 130}
                                     MarginTop={5}>
                            {/*<CustomText weight='bold' style={{fontSize: 18,}}></CustomText>*/}
                        </CustomInput>
                    </View>

                    {/*------------------itemCount--------------*/}

                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <CustomText weight='semi' style={{
                            fontSize: 12,
                            opacity: 0.75,
                            color: constant.clrBlack,
                            marginTop: 9
                        }}>
                            count
                        </CustomText>
                        <View style={{
                            position: 'relative',
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 5,
                            width: constant.windowWidth - 280,
                            backgroundColor: '#EEEEEE',
                            borderRadius: 50,
                            height: 42,
                        }}>
                            <TouchableOpacity onPress={() => DECREASE_COUNT()}
                                              style={[styles.countBtn, {
                                                  position: 'absolute',
                                                  left: 0,
                                                  borderTopLeftRadius: 50,
                                                  borderBottomLeftRadius: 50,
                                              }]}>
                                <CustomText weight='bold' style={{fontSize: 14,}}>-</CustomText>
                            </TouchableOpacity>
                            <CustomText weight='bold' style={{fontSize: 14,}}>{fields.itemCount}</CustomText>
                            <TouchableOpacity onPress={() => INCREASE_COUNT()}
                                              style={[styles.countBtn, {
                                                  position: 'absolute',
                                                  right: 0,
                                                  borderTopRightRadius: 50,
                                                  borderBottomRightRadius: 50,
                                              }]}>
                                <CustomText weight='bold' style={{fontSize: 14,}}>+</CustomText>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {/*------------------itemUnit--------------*/}
                    <CustomBtn onPress={() => fieldHandler('itemUnit', 'pkg')}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               textClr={'black'}
                               btnWidth={((constant.windowWidth - 30) / 4) - 10}
                               opacity={fields.itemUnit === 'pkg' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>pkg</CustomText>
                    </CustomBtn>

                    <CustomBtn onPress={() => fieldHandler('itemUnit', 'kg')}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               textClr={'black'}
                               btnWidth={((constant.windowWidth - 30) / 4) - 10}
                               opacity={fields.itemUnit === 'kg' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>kg</CustomText>
                    </CustomBtn>

                    <CustomBtn onPress={() => fieldHandler('itemUnit', 'litre')}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               textClr={'black'}
                               btnWidth={((constant.windowWidth - 30) / 4) - 10}
                               opacity={fields.itemUnit === 'litre' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>litre</CustomText>
                    </CustomBtn>

                    <CustomBtn onPress={() => fieldHandler('itemUnit', 'bott')}
                               MargBotNone
                               btnClr={'#EEEEEE'}
                               textClr={'black'}
                               btnWidth={((constant.windowWidth - 30) / 4) - 10}
                               opacity={fields.itemUnit === 'bott' ? 1 : 0.5}>
                        <CustomText weight='bold' style={{fontSize: 12,}}>bott</CustomText>
                    </CustomBtn>

                    {/*------------------SubmitButton--------------*/}
                    <CustomBtn onPress={() => addItem()} btnWidth={constant.windowWidth - 30}>
                        <CustomText weight='bold'>ADD TO LIST</CustomText>
                    </CustomBtn>

                </View>

                {/*------------------itemsList--------------*/}
                <View style={styles.contentLine}/>
                <ScrollView style={{marginBottom: 30}}>
                    {
                        shopList.shopListItems.map((listItem) => (
                            <SingleListEditItem key={listItem.id} name={listItem.name} amount={listItem.amount}
                                                unit={listItem.unit} isBought={listItem.isBought}/>
                        ))
                    }
                </ScrollView>
            </View>
        </View>

    )
});

const styles = StyleSheet.create({
        header: {
            backgroundColor: constant.clrOrange,
            height: constant.windowHeight / 2,
            width: constant.windowWidth,
        },
        content: {
            position: 'absolute',
            flex: 1,
            alignItems: 'center',
            backgroundColor: constant.clrWhite,
            height: constant.windowHeight,
            width: constant.windowWidth,
            borderTopEndRadius: 20,
            borderTopStartRadius: 20,
            top: 70,
            paddingTop: 10,
        },

        countBtn: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#EEEEEE',
            height: 42,
            width: 30,
            zIndex: 2,
        },

        backIcon: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            width: 100,
            height: 80,
            top: -3,
            left: -22,
        },

        editIcon: {
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            color: 'black',
            width: 100,
            height: 80,
            top: -3,
            right: -22,
        },

        headerText: {
            fontSize: 18,
            textAlign: "center",
            color: constant.clrWhite,
            paddingTop: 25,
            lineHeight: 22,
        },

        contentView: {
            width: constant.windowWidth - 30,
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            alignItems: 'center',
        },

        contentBtn: {
            backgroundColor: constant.clrOrange,
            display: "flex",
            justifyContent: 'center',
            alignItems: 'center',
            width: 72,
            height: 20,
            borderRadius: 50,
        },

        contentLine: {
            width: constant.windowWidth,
            height: 2,
            backgroundColor: '#E5E5E5',
            marginBottom: 19,
        }
    })
;