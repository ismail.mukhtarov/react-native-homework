import {Dimensions} from "react-native";

export const clrOrange = '#FF7676';
export const clrLightYellow = '#FFE194';
export const clrYellow = "#FFD976";
export const clrBlack = "#303234";
export const clrWhite = "#FFFFFF";
export const clrGrey = "#EEEEEE";

export const windowWidth = Dimensions.get("window").width;
export const windowHeight = Dimensions.get("window").height;
