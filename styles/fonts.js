import * as Font from 'expo-font';

import MontserratBold from '../assets/fonts/Montserrat-Bold.ttf'
import MontserratSemiBold from '../assets/fonts/Montserrat-SemiBold.ttf'
import MontserratMedium from '../assets/fonts/Montserrat-Medium.ttf'

export const loadFonts = () => {
    return Font.loadAsync({
        MontserratBold,
        MontserratSemiBold,
        MontserratMedium,
    })
};