import React from 'react';
import {StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';

import userImage from '../assets/userImage.png'
import * as constant from "../styles/constants";
import {clrWhite} from "../styles/constants";
import {clrOrange} from "../styles/constants";
import {CustomText} from "./CustomText";
import {getUser} from "../store/settings";
import {connect} from "react-redux";

const mapStateToProps = (state) => ({
    user: getUser(state),
});

export const LeftDrawer = connect(mapStateToProps)((props) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                {
                    props.user.url ?
                        <Image style={[styles.img, {borderWidth: 3,}]}
                               source={{uri: `${props.user.url}`}}/>
                        :
                        <Image style={styles.img} source={userImage}/>
                }

                <CustomText style={styles.headerText}>{props.user.name}</CustomText>
                {/*<CustomText style={styles.headerText}>{props.user.url}</CustomText>*/}
            </View>
            <View style={styles.content}>

                <TouchableOpacity onPress={() => props.navigation.navigate('NewList')}
                                  style={[styles.btn, {
                                      marginTop: 26,
                                      marginBottom: 36,
                                  }]}>
                    <CustomText weight='bold' style={styles.customText}>ADD NEW LIST</CustomText>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.navigation.navigate('OneTime')}
                                  style={styles.btn}>
                    <CustomText weight='bold' style={styles.customText}>ONE TIME LIST</CustomText>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.navigation.navigate('Regular')}
                                  style={styles.btn}>
                    <CustomText weight='bold' style={styles.customText}>REGULAR LIST</CustomText>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.navigation.navigate('UserSettings')}
                                  style={styles.btn}>
                    <CustomText weight='bold' style={styles.customText}>USER SETTINGS</CustomText>
                </TouchableOpacity>

            </View>
        </View>
    )
});

const styles = StyleSheet.create({
    header: {
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: constant.clrWhite,
        height: constant.windowHeight / 2,
        width: '100%',
    },
    content: {
        position: 'absolute',
        flex: 1,
        alignItems: 'center',
        backgroundColor: constant.clrOrange,
        height: constant.windowHeight,
        width: '100%',
        borderTopEndRadius: 20,
        borderTopStartRadius: 20,
        top: 70,
    },

    img: {
        marginTop: 13,
        marginLeft: 16,
        width: 50,
        height: 50,
        borderRadius: 100,
        borderColor: constant.clrOrange,
    },

    headerText: {
        color: '#303234',
        opacity: 0.65,
        marginTop: 30,
        marginLeft: 22,
        fontSize: 24,
        lineHeight: 29,
    },

    btn: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 251,
        height: 34,
        backgroundColor: clrWhite,
        borderRadius: 38,
        marginTop: 10,
    },

    customText: {
        color: clrOrange,
        fontSize: 14,
    }

});