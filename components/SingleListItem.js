import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {clrYellow, clrWhite, windowWidth} from '../styles/constants';
import {CustomText} from "./CustomText";

export const SingleListItem = ({name, amount, unit, isBought, onLongPress}) => {
    return (
        <View style={{opacity: isBought ? 0.5 : 1}}>
            <TouchableOpacity onLongPress={onLongPress} style={styles.listItem}>
                <CustomText>{name}</CustomText>
                <CustomText>x{amount} {unit}</CustomText>
            </TouchableOpacity>
        </View>

    )


};

const styles = StyleSheet.create({
    listItem: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        width: windowWidth - 30,
        backgroundColor: clrWhite,
        borderWidth: 2,
        borderColor: clrYellow,
        borderRadius: 27,
        marginTop: 14,
        paddingHorizontal: 20,

    },

});
