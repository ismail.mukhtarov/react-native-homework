import React from 'react';
import {StyleSheet, View, TouchableOpacity, Alert} from 'react-native';
import {connect} from "react-redux";

import * as constant from "../styles/constants";
import {CustomText} from "./CustomText";


export const CardsList = (props) => {

    return (
        <View>
            {
                props.section.shopList.map((shopList) => {
                        const totalNumber = shopList.shopListItems.length,
                            boughtNumber = shopList.shopListItems.filter((item) => item.isBought).length;
                        return (
                            <View key={shopList.id}
                                  style={{opacity: props.allBoughtOpacity ? (totalNumber === boughtNumber ? 0.5 : 1) : 1}}>
                                <TouchableOpacity key={shopList.id}
                                                  onLongPress={() => props.deleteSectionList(shopList.name, shopList.id)}
                                                  onPress={() => props.navigation.navigate('SingleList', {
                                                      shopListSection: props.section,
                                                      shopList: shopList,
                                                      totalNumber,
                                                      boughtNumber
                                                  })}
                                >
                                    <View style={styles.listCard}>
                                        <View style={styles.listCardView}>
                                            <CustomText weight='semi' style={{fontSize: 18,}}>{shopList.name}</CustomText>
                                            <CustomText weight='semi'
                                                        style={{fontSize: 14,}}>{boughtNumber} / {totalNumber}</CustomText>
                                        </View>
                                        <View key={shopList.id} style={styles.progressBar}>
                                            <View style={[styles.progressBarLine,
                                                {
                                                    width: (300 * boughtNumber) / totalNumber || 0,
                                                    backgroundColor: '#FFD976',
                                                }]}/>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        )
                    }
                )
            }
        </View>


    )
};

const styles = StyleSheet.create({
    listCard: {
        width: constant.windowWidth - 30,
        height: 76,
        backgroundColor: '#FFFFFF',
        borderColor: '#FFD976',
        borderWidth: 2,
        borderRadius: 10,
        marginTop: 15,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },

    listCardView: {
        display: 'flex',
        width: 300,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },

    progressBar: {
        width: 300,
        height: 20,
        backgroundColor: '#EEEEEE',
        color: 'black',
        borderRadius: 10,
    },

    progressBarLine: {
        height: 20,
        color: 'black',
        borderRadius: 10,
    }
});