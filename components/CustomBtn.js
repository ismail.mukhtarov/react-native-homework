import React from 'react';
import {StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import {clrOrange, clrWhite} from '../styles/constants';

export const CustomBtn = (props) => {
    return (
        <View style={{opacity: props.opacity ? props.opacity : 1,}}>
            <TouchableOpacity onPress={props.onPress}
                              style={[styles.btn,
                                  {
                                      width: props.btnWidth,
                                      backgroundColor: props.btnClr ? props.btnClr : clrOrange,
                                      marginBottom: props.MargBotNone ? 0 : 21,
                                      marginTop: props.MarginTop ? props.MarginTop : 14,
                                  }]}>
                <Text style={[styles.btnTxt, {color: props.textClr ? props.textClr : clrWhite}]}>{props.children}</Text>
            </TouchableOpacity>
        </View>

    )


};

const styles = StyleSheet.create({
    btn: {
        height: 42,
        borderRadius: 40,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: clrOrange,
    },

    btnTxt: {
        fontSize: 14,
        lineHeight: 17,
    }
});
