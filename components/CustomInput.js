import React from 'react';
import {TextInput, StyleSheet} from 'react-native';


export const CustomInput = ({btnWidth, children, action, valueName, MarginTop}) => {
    return (
        <TextInput onChangeText={(text) => action ? action(valueName, text) : null}
                   style={[styles.textInput, {width: btnWidth, marginTop: MarginTop ? MarginTop : 10,}]}
                   placeholderTextColor="black"
                   value={children}
        />
    )
};

const styles = StyleSheet.create({
    textInput: {
        height: 42,
        textAlign: 'center',
        borderRadius: 45,
        backgroundColor: '#EEEEEE',
        fontSize: 15,
        paddingRight: 10,
        paddingLeft: 10,
    }
});
