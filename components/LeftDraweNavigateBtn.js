import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';

export const LeftDrawerNavigateBtn = (props) => {
    return (
        <TouchableOpacity style={styles.btn}
                          onPress={props.onPress}
        >
            <View style={styles.btnLine}/>
            <View style={styles.btnLine}/>
            <View style={styles.btnLine}/>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    btn: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 26,
        right: 16,
    },

    btnLine: {
        width: 28,
        height: 2,
        backgroundColor: '#FFFFFF',
        marginTop: 3,
        marginBottom: 3,
    }
});
