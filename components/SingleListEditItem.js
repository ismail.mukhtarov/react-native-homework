import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, View, ScrollView} from 'react-native';
import {AntDesign, MaterialIcons} from '@expo/vector-icons';


import {clrYellow, clrWhite, windowWidth, clrOrange} from '../styles/constants';
import {CustomText} from "./CustomText";

export class SingleListEditItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

            <View style={[styles.listItem, {opacity: this.props.isBought ? 0.5 : 1}]}>
                <TouchableOpacity style={styles.editIconView}>
                    <MaterialIcons name="edit" size={24} color={clrWhite}/>
                </TouchableOpacity>
                <CustomText>{this.props.name}</CustomText>
                <CustomText>{this.props.amount} {this.props.unit}</CustomText>
                <TouchableOpacity style={styles.deleteIconView}>
                    <AntDesign name="close" size={30} color={clrWhite}/>
                </TouchableOpacity>
            </View>


        )
    }

}


const styles = StyleSheet.create({
    listItem: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 40,
        width: windowWidth - 30,
        backgroundColor: clrWhite,
        borderWidth: 2,
        borderColor: clrYellow,
        borderRadius: 27,
        marginTop: 14,
        paddingHorizontal: 57,

    },
    editIconView: {
        position: 'absolute',
        left: -1,
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: clrYellow,
        borderRadius: 50,
    },

    deleteIconView: {
        width: 40,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: clrOrange,
        position: 'absolute',
        right: 0,
        borderRadius: 50,
    },


});

