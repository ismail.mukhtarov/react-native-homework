import React from "react";
import {Text} from "react-native";

const fontFamilies = {
    bold: "MontserratBold",
    semi: "MontserratSemiBold",
    medium: "MontserratMedium",
};

export const CustomText = ({children, style, weight, ...rest}) => {
    return (
        <Text
            {...rest}
            style={[
                {
                    fontFamily: fontFamilies[weight] || fontFamilies.medium,
                },
                style,
            ]}
        >
            {children}
        </Text>
    );
};
