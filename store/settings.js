//action types
const UPDATE_USER = "UPDATE_USER";

//selector

export const MODULE_NAME = "settings";
export const getUser = (state) => state[MODULE_NAME].user;


const initialState = {
    user: {
        name: "i.mukhtarov",
        url: '',
    }
};

//reducer
export function settingsReducer(state = initialState, {type, payLoad}) {
    switch (type) {
        case UPDATE_USER:
            return {
                ...state,
                user: {
                    ...state.user,
                    name: payLoad.name ? payLoad.name : state.user.name,
                    url: payLoad.url ? payLoad.url : state.user.url,
                }
            };
        default:
            return state;
    }
}

//action creators
export const updateUser = (payLoad) => ({
    type: UPDATE_USER,
    payLoad
});
