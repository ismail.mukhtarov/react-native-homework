//action types
const ADD_LIST = "ADD_LIST";
const ADD_ITEM_TO_LIST = "ADD_ITEM_TO_LIST";
const DELETE_LIST = "DELETE_LIST";
const CHANGE_ITEM_BOUGHT = "CHANGE_ITEM_BOUGHT";

const RESET_SHOPPING = "RESET_SHOPPING";
const DELETE_ITEM = "DELETE_ITEM";
const UPDATE_ITEM = "UPDATE_ITEM";

//selector

export const MODULE_NAME = "shop";
export const getDatum = (state) => state[MODULE_NAME];
export const getShopLists = (state) => state[MODULE_NAME].shopListSections;

const initialState = {
    shopListSections: [
        {
            id: `${Math.random()}${Date.now()}`,
            name: "ONE_TIME",
            shopList: [
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: 'Everything for breakfast',
                    numberOfBought: 1,
                    shopListItems: [
                        {id: `${Math.random()}${Date.now()}`, name: 'Pasta', unit: 'pkg', amount: 2, isBought: false,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Salt', unit: 'pkg', amount: 1, isBought: false,},
                        {
                            id: `${Math.random()}${Date.now()}`,
                            name: 'Tomatoes',
                            unit: 'kg',
                            amount: 1,
                            isBought: false,
                        },
                        {id: `${Math.random()}${Date.now()}`, name: 'Cheese', unit: 'kg', amount: 2, isBought: true,},
                    ],
                },
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: 'Everything with pasta',
                    numberOfBought: 1,
                    shopListItems: [
                        {id: `${Math.random()}${Date.now()}`, name: 'Pasta', unit: 'pkg', amount: 2, isBought: false,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Salt', unit: 'pkg', amount: 1, isBought: false,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Tomatoes', unit: 'kg', amount: 1, isBought: true,},
                    ],
                },
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: 'Kitchen repair',
                    numberOfBought: 3,
                    shopListItems: [
                        {id: `${Math.random()}${Date.now()}`, name: 'Tomatoes', unit: 'kg', amount: 1, isBought: true,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Cheese', unit: 'kg', amount: 2, isBought: true,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Tomatoes', unit: 'kg', amount: 1, isBought: true,},
                    ],
                }]
        },
        {
            id: `${Math.random()}${Date.now()}`,
            name: "REGULAR",
            shopList: [
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: 'Everything with pasta',
                    numberOfBought: 1,
                    shopListItems: [
                        {id: `${Math.random()}${Date.now()}`, name: 'Pasta', unit: 'pkg', amount: 2, isBought: false,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Salt', unit: 'pkg', amount: 1, isBought: false,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Tomatoes', unit: 'kg', amount: 1, isBought: true,},
                    ],
                },
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: 'Kitchen repair',
                    numberOfBought: 2,
                    shopListItems: [
                        {id: `${Math.random()}${Date.now()}`, name: 'Tomatoes', unit: 'kg', amount: 1, isBought: true,},
                        {id: `${Math.random()}${Date.now()}`, name: 'Cheese', unit: 'kg', amount: 2, isBought: true,},
                    ],
                }]
        },
    ],
};

//reducer
export function shopListReducer(state = initialState, {type, payLoad}) {
    switch (type) {
        case ADD_LIST:
            const updatedState = {...state};
            updatedState.shopListSections = [...updatedState.shopListSections];
            const selectedSectionIndex = updatedState.shopListSections.findIndex((section) => section.name === payLoad.sectionName);
            // console.log(selectedSectionIndex);
            updatedState.shopListSections[selectedSectionIndex].shopList = [
                ...updatedState.shopListSections[selectedSectionIndex].shopList,
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: payLoad.listName,
                    numberOfBought: 0,
                    shopListItems: [],
                }
            ];
            return updatedState;

        case ADD_ITEM_TO_LIST:
            const updatedItems = {...state};
            updatedItems.shopListSections = [...updatedItems.shopListSections];
            const selectedShopListsSectionIndex = updatedItems.shopListSections.findIndex((section) => section.id === payLoad.shopListSectionID);
            const updatedShopLists = [...updatedItems.shopListSections[selectedShopListsSectionIndex].shopList],
                shopListIndex = updatedShopLists.findIndex((singleShopList) => singleShopList.id === payLoad.shopListID);
            console.log(updatedShopLists[shopListIndex].shopListItems);

            updatedShopLists[shopListIndex].shopListItems = [
                {
                    id: `${Math.random()}${Date.now()}`,
                    name: payLoad.itemName,
                    unit: payLoad.itemUnit,
                    amount: payLoad.itemCount,
                    isBought: false,
                },
                ...updatedShopLists[shopListIndex].shopListItems
            ];
            return updatedItems;

        case DELETE_LIST:
            return {
                ...state,
                shopListSections: state.shopListSections.map((section) => {
                    console.log('hello');
                    if (section.name === payLoad?.sectionName) {
                        return {
                            ...section,
                            shopList: section.shopList.filter((list) => list.id !== payLoad.listId)
                        }
                    }
                    return section;
                })
            };

        case CHANGE_ITEM_BOUGHT:
            return {
                ...state,
                shopListSections: state.shopListSections.map((section) => {
                    if (section.name === payLoad.sectionName) {
                        return {
                            ...section,
                            shopList: section.shopList.map((list) => {
                                if (list.id === payLoad.listId) {
                                    return {
                                        ...list,
                                        shopListItems: list.shopListItems.map((item) => {
                                            if (item.id === payLoad.listItemId) {
                                                return {
                                                    ...item,
                                                    isBought: !item.isBought
                                                }
                                            }
                                            return item;
                                        })

                                    }
                                }
                                return list;
                            })
                        }
                    }
                    return section;
                })
            };

        case RESET_SHOPPING:
            return {
                ...state,
                shopList: state.shopList.map(shopItem => {
                    if (shopItem.id === payLoad.idShopItem) {
                        return {
                            ...shopItem,
                            items: shopItem.items.map(item => {
                                return {
                                    ...item,
                                    isBought: false,
                                }
                            })
                        }
                    }
                    return shopItem;
                })
            };
        case DELETE_ITEM:
            return {
                ...state,
                shopList: state.shopList.map(shopItem => {
                    if (shopItem.id === payLoad.idShopItem) {
                        return {
                            ...shopItem,
                            items: shopItem.items.filter(item => item.id !== payLoad.idItem)
                        }
                    }
                    return shopItem;
                })
            };
        case UPDATE_ITEM:
            return {
                ...state,
                shopList: state.shopList.map(shopItem => {
                    if (shopItem.id === payLoad.idShopItem) {
                        return {
                            ...shopItem,
                            items: shopItem.items.map(item => {
                                if (item.id === payLoad.item.id) {
                                    return {
                                        ...item,
                                        ...payLoad.item,
                                    }
                                }
                                return item;
                            })
                        }
                    }
                    return shopItem;
                })
            };
        default:
            return state;
    }
}

//action creators
export const addList = (payLoad) => ({
    type: ADD_LIST,
    payLoad
});
export const addItemToList = (payLoad) => ({
    type: ADD_ITEM_TO_LIST,
    payLoad
});
export const deleteList = (payLoad) => ({
    type: DELETE_LIST,
    payLoad
});
export const changeItemBought = (payLoad) => ({
    type: CHANGE_ITEM_BOUGHT,
    payLoad
});

export const updateItem = (payLoad) => ({
    type: UPDATE_ITEM,
    payLoad
});
export const deleteItem = (payLoad) => ({
    type: DELETE_ITEM,
    payLoad
});
export const resetShopping = (payLoad) => ({
    type: RESET_SHOPPING,
    payLoad
});