import {createStore, combineReducers} from 'redux'
import {shopListReducer} from './project'
import {settingsReducer} from "./settings";

const store = createStore(
    combineReducers({
        shop: shopListReducer,
        settings: settingsReducer
    })
);

export default store;